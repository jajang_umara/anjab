-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 23, 2018 at 04:29 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anjab`
--

-- --------------------------------------------------------

--
-- Table structure for table `anjab`
--

CREATE TABLE `anjab` (
  `id` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `bahan_kerja` text NOT NULL,
  `perangkat_kerja` text NOT NULL,
  `korelasi_jabatan` varchar(1000) NOT NULL,
  `tanggung_jawab` varchar(1000) NOT NULL,
  `wewenang` varchar(1000) NOT NULL,
  `resiko_berbahaya` varchar(1000) NOT NULL,
  `prestasi_kerja` varchar(255) NOT NULL,
  `kelas_jabatan` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `jpt_madya` int(11) NOT NULL,
  `jpt_pratama` int(11) NOT NULL,
  `administrator` int(11) NOT NULL,
  `pengawas` int(11) NOT NULL,
  `pelaksana` int(11) NOT NULL,
  `keterampilan_kerja` varchar(1000) NOT NULL,
  `bakat_kerja` varchar(1000) NOT NULL,
  `temperamen_kerja` varchar(1000) NOT NULL,
  `minat_kerja` varchar(1000) NOT NULL,
  `upaya_fisik` varchar(1000) NOT NULL,
  `kondisi_fisik` varchar(1000) NOT NULL,
  `fungsi_pekerjaan` varchar(1000) NOT NULL,
  `jumlah_kebutuhan` int(11) NOT NULL,
  `jumlah_saat_ini` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anjab`
--

INSERT INTO `anjab` (`id`, `id_jabatan`, `id_unit_kerja`, `bahan_kerja`, `perangkat_kerja`, `korelasi_jabatan`, `tanggung_jawab`, `wewenang`, `resiko_berbahaya`, `prestasi_kerja`, `kelas_jabatan`, `status`, `jpt_madya`, `jpt_pratama`, `administrator`, `pengawas`, `pelaksana`, `keterampilan_kerja`, `bakat_kerja`, `temperamen_kerja`, `minat_kerja`, `upaya_fisik`, `kondisi_fisik`, `fungsi_pekerjaan`, `jumlah_kebutuhan`, `jumlah_saat_ini`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '[{"bahan_kerja":"asasas","penggunaan":"ffgfgfg"},{"bahan_kerja":"1234","penggunaan":"111111"}]', '[{"perangkat_kerja":"qwqwqwr","penggunaan_perangkat":"rrrrrrrr"},{"perangkat_kerja":"rrr","penggunaan_perangkat":"rrr"}]', '[{"korelasi_jabatan":"2","korelasi_unit_kerja":"1","hal":"pertukaran data"}]', '[{"tanggung_jawab":"Keberhasilan pelaksanaan tugas."},{"tanggung_jawab":"rtrtrt"}]', '[{"wewenang":"Keberhasilan pelaksanaan tugas."},{"wewenang":"rtrtrtrt"}]', '[{"fisik_mental":"rtrt","penyebab":"rtrtrtr"}]', 'sangat baik', '', 0, 1, 2, 3, 4, 0, '["1","2"]', '["1","2"]', '["1"]', '["1","2"]', '["1","2"]', '{"jenis_kelamin":"Wanita","umur":"50 tahun","tinggi_badan":"172","berat_badan":"56","postur_badan":"","penampilan":""}', '{"data":"1","orang":"1","benda":"3"}', 0, 0, '2018-08-18 15:12:05', '2018-08-18 15:12:05');

-- --------------------------------------------------------

--
-- Table structure for table `master_bakat_kerja`
--

CREATE TABLE `master_bakat_kerja` (
  `id` int(11) NOT NULL,
  `kode` varchar(2) NOT NULL,
  `bakat` varchar(255) NOT NULL,
  `keterangan` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_bakat_kerja`
--

INSERT INTO `master_bakat_kerja` (`id`, `kode`, `bakat`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'G', 'intelegensia', 'Kemampuan belajar secara umum.', '2018-08-14 05:59:12', '0000-00-00 00:00:00'),
(2, 'V', 'bakat verbal', 'Kemampuan untuk memahami arti kata-kata dan penggunaannya secara tepat dan efektif.', '2018-08-14 06:21:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_fungsi_fisik`
--

CREATE TABLE `master_fungsi_fisik` (
  `id` int(11) NOT NULL,
  `kode` varchar(2) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `keterangan` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_fungsi_fisik`
--

INSERT INTO `master_fungsi_fisik` (`id`, `kode`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'B0', 'Memasang mesin', 'Menyesuaikan mesin untuk melakukan suatu pekerjaan tertentu dengan mema­sang, mengubah komponen-komponennya atau memperbaiki mesin menurut standar.', '2018-08-07 06:35:03', '0000-00-00 00:00:00'),
(2, 'B1', 'Mengerjakan persisi', 'Menggunakan anggota badan atau perkakas untuk mengerjakan, memindahkan, menga-rahkan atau menempatkan obyek secara tepat sesuai dengan standar yang telah ditetapkan dengan toleransi yang kecil.', '2018-08-14 07:21:25', '2018-08-14 07:21:25'),
(3, 'B2', 'Menjalankan - mengontrol mesin', 'Menghidupkan, menyetel, mengatur kerja dan menghentikan mesin serta mengamati berbagai alat petunjuk pada mesin.', '2018-08-14 07:21:55', '0000-00-00 00:00:00'),
(4, 'D0', 'Memadukan data', 'Menyatukan atau memadukan hasil analisis data untuk menemukan fakta menyusun karangan atau mengembangkan konsep, pengetahuan, interprestasi, menciptakan gagasan dengan menggunakan imajinasi.\r\n', '2018-08-14 07:22:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_jabatan`
--

CREATE TABLE `master_jabatan` (
  `id` int(11) NOT NULL,
  `kode_jabatan` varchar(100) NOT NULL,
  `nama_jabatan` varchar(200) NOT NULL,
  `deskripsi_jabatan` text NOT NULL,
  `eselon` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_jabatan`
--

INSERT INTO `master_jabatan` (`id`, `kode_jabatan`, `nama_jabatan`, `deskripsi_jabatan`, `eselon`, `created_at`, `updated_at`) VALUES
(1, 'ORG 2.3.2', 'Pengolah Data Kelembagaan', 'Melakukan Kegiatan pengumpulan, pendokumentasian/penginputan dan pengolahan di bidang data kelembagaan', 'IVA', '2018-08-18 16:35:56', '2018-08-18 16:35:56'),
(2, 'ORG   3.2.2', 'Pengelola Data       ', 'Melakukan Kegiatan pengelolaan yang meliputi penyiapan bahan, koordinasi dan penyusunan laporan untuk disajikan kepada pimpinan      \r\n', 'IVA', '2018-08-07 02:33:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_keterampilan_kerja`
--

CREATE TABLE `master_keterampilan_kerja` (
  `id` int(11) NOT NULL,
  `keterampilan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_keterampilan_kerja`
--

INSERT INTO `master_keterampilan_kerja` (`id`, `keterampilan`, `created_at`, `updated_at`) VALUES
(1, 'Keterampilan komputer dan Aplikasi', '2018-08-07 03:24:41', '2018-08-07 03:24:41'),
(2, 'Kemampuan analisis dan evaluasi data', '2018-08-14 05:00:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_kondisi_lingkungan_kerja`
--

CREATE TABLE `master_kondisi_lingkungan_kerja` (
  `id` int(11) NOT NULL,
  `eselon` varchar(10) NOT NULL,
  `aspek` varchar(555) NOT NULL,
  `faktor` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kondisi_lingkungan_kerja`
--

INSERT INTO `master_kondisi_lingkungan_kerja` (`id`, `eselon`, `aspek`, `faktor`, `created_at`, `updated_at`) VALUES
(1, 'IVA', 'Tempat Kerja 1', 'Didalam dan diluar ruangan', '2018-08-18 16:31:37', '2018-08-18 16:31:37'),
(2, 'IVA', 'Suhu', 'Sejuk', '2018-08-18 16:30:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_minat_kerja`
--

CREATE TABLE `master_minat_kerja` (
  `id` int(11) NOT NULL,
  `kode` varchar(4) NOT NULL,
  `keterangan` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_minat_kerja`
--

INSERT INTO `master_minat_kerja` (`id`, `kode`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '1A', 'Pilihan melakukan kegiatan-kegiatan yang berhubungan dengan benda-benda dan obyek-obyek.', '2018-08-07 04:45:48', '0000-00-00 00:00:00'),
(2, '1B', 'Pilihan melakukan kegiatan yang berhubungan dengan komunikasi data.', '2018-08-14 06:34:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_syarat_jabatan`
--

CREATE TABLE `master_syarat_jabatan` (
  `id` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `syarat_jabatan` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_syarat_jabatan`
--

INSERT INTO `master_syarat_jabatan` (`id`, `id_jabatan`, `jenis`, `syarat_jabatan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pendidikan', 'Menimal Diploma III  di bidang Teknik Informatika/Manajemen Teknik Informatika/Administrasi Perkantoran/Manajemen atau bidang lain yang relevan dengan tugas jabatan ', '2018-08-07 01:35:34', '2018-08-07 01:35:34'),
(2, 1, 'Diklat', 'Diklat Kebijakan Pemerintah ', '2018-08-07 01:36:10', '2018-08-07 01:36:10'),
(3, 1, 'Diklat', 'Diklat Aplikasi Komputer', '2018-08-18 12:20:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_temperamen_kerja`
--

CREATE TABLE `master_temperamen_kerja` (
  `id` int(11) NOT NULL,
  `kode` varchar(2) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `keterangan` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_temperamen_kerja`
--

INSERT INTO `master_temperamen_kerja` (`id`, `kode`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'D', 'Directing -Control-Planning (DCP) ', 'Kemampuan menyesuaikan diri menerima tanggung jawab untuk kegiatan memimpin, mengendalikan atau merencanakan.', '2018-08-07 04:08:38', '2018-08-07 04:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `master_tugas_pokok_jabatan`
--

CREATE TABLE `master_tugas_pokok_jabatan` (
  `id` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `tugas_pokok` text NOT NULL,
  `hasil_kerja` varchar(100) NOT NULL,
  `jumlah_beban` float NOT NULL,
  `waktu_penyelesaian` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_tugas_pokok_jabatan`
--

INSERT INTO `master_tugas_pokok_jabatan` (`id`, `id_jabatan`, `tugas_pokok`, `hasil_kerja`, `jumlah_beban`, `waktu_penyelesaian`, `created_at`, `updated_at`) VALUES
(1, 1, 'Menerima dan memeriksa bahan dan Data  Kelembagaan  sesuai dengan  prosedur sebagai kajian dalam rangka data Kelembagaan', 'Data', 235, 5, '2018-08-07 02:14:27', '2018-08-07 02:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `master_unit_kerja`
--

CREATE TABLE `master_unit_kerja` (
  `id` int(11) NOT NULL,
  `unit_kerja` varchar(200) NOT NULL,
  `parent` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_unit_kerja`
--

INSERT INTO `master_unit_kerja` (`id`, `unit_kerja`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'Sekretariat Daerah Provinsi Jambi', 0, '2018-08-11 02:49:18', '0000-00-00 00:00:00'),
(2, 'Biro Organisasi Setda Provinsi Jambi.', 1, '2018-08-11 02:49:31', '0000-00-00 00:00:00'),
(3, 'Kepala Bagian Kelembagaan ', 2, '2018-08-11 02:50:20', '0000-00-00 00:00:00'),
(4, 'Kepala Subbag Kelembagaan Kab/Kota', 3, '2018-08-11 02:50:44', '0000-00-00 00:00:00'),
(5, 'Sekretariat Provinsi jawa barat', 0, '2018-08-16 06:20:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `master_upaya_fisik`
--

CREATE TABLE `master_upaya_fisik` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` varchar(555) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_upaya_fisik`
--

INSERT INTO `master_upaya_fisik` (`id`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Berdiri', 'Berada di suatu tern pat dalam posisi tegak ditempat tanpa pindah ke tempat lain.', '2018-08-07 06:05:33', '2018-08-07 06:05:33'),
(2, 'berjalan', 'Bergerak dengan jalan kaki.', '2018-08-14 07:01:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `id_unit_kerja`, `username`, `password`, `nama`, `role`, `created_at`, `updated_at`) VALUES
(1, 0, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 1, '2018-08-23 15:13:24', '2018-08-23 15:13:24'),
(2, 1, 'users', '5b7dcd14a4faa2cdd54cf6eb8d4bc35da31914a1', 'User-1', 2, '2018-08-23 15:15:38', '2018-08-23 15:15:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anjab`
--
ALTER TABLE `anjab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_bakat_kerja`
--
ALTER TABLE `master_bakat_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_fungsi_fisik`
--
ALTER TABLE `master_fungsi_fisik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_jabatan`
--
ALTER TABLE `master_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_keterampilan_kerja`
--
ALTER TABLE `master_keterampilan_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_kondisi_lingkungan_kerja`
--
ALTER TABLE `master_kondisi_lingkungan_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_minat_kerja`
--
ALTER TABLE `master_minat_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_syarat_jabatan`
--
ALTER TABLE `master_syarat_jabatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `master_temperamen_kerja`
--
ALTER TABLE `master_temperamen_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_tugas_pokok_jabatan`
--
ALTER TABLE `master_tugas_pokok_jabatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jabatan` (`id_jabatan`);

--
-- Indexes for table `master_unit_kerja`
--
ALTER TABLE `master_unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_upaya_fisik`
--
ALTER TABLE `master_upaya_fisik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anjab`
--
ALTER TABLE `anjab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_bakat_kerja`
--
ALTER TABLE `master_bakat_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_fungsi_fisik`
--
ALTER TABLE `master_fungsi_fisik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `master_jabatan`
--
ALTER TABLE `master_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_keterampilan_kerja`
--
ALTER TABLE `master_keterampilan_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_kondisi_lingkungan_kerja`
--
ALTER TABLE `master_kondisi_lingkungan_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_minat_kerja`
--
ALTER TABLE `master_minat_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_syarat_jabatan`
--
ALTER TABLE `master_syarat_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_temperamen_kerja`
--
ALTER TABLE `master_temperamen_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_tugas_pokok_jabatan`
--
ALTER TABLE `master_tugas_pokok_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_unit_kerja`
--
ALTER TABLE `master_unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `master_upaya_fisik`
--
ALTER TABLE `master_upaya_fisik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `master_syarat_jabatan`
--
ALTER TABLE `master_syarat_jabatan`
  ADD CONSTRAINT `master_syarat_jabatan_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `master_jabatan` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `master_tugas_pokok_jabatan`
--
ALTER TABLE `master_tugas_pokok_jabatan`
  ADD CONSTRAINT `master_tugas_pokok_jabatan_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `master_jabatan` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
