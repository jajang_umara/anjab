<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends ADMIN_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	
	function peta()
	{
		$where_unit = [];
		if (session('role') == 2){
			$where_unit['id'] = session('unit_id');
		}
		$data['unit_kerja'] = $this->m_unit_kerja->where_parent(0)->where($where_unit)->dropdown('id','unit_kerja');
		$this->load_admin('index',$data);
	}
	
	function peta_jabatan($unit_kerja)
	{
		$data['tree'] = buildTree($this->m_anjab->get_parent_child($unit_kerja));
		$this->load->view('map',$data);
	}
}