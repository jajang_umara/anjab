<form id="fm" class="form-horizontal form-label-left">
	<?= _input('id',[],$id,'hidden'); ?>
	<div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jabatan <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<?= generate_select_input($jabatan,'--Pilih Jabatan--',["name"=>"id_jabatan","class"=>"form-control"],@$row->id_jabatan); ?>
		</div>
    </div>
	<div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tugas Pokok <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<?= textarea('tugas_pokok',['class'=>'form-control col-md-12 col-xs-12'],@$row->tugas_pokok); ?>
		</div>
    </div>
	<div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Hasil Kerja <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<?= generate_select_input(['Data'=>'Data',
									   'Kegiatan'=>'Kegiatan',
									   'Laporan'=>'Laporan']
									 ,'--Pilih Hasil Kerja--',
									 ["name"=>"hasil_kerja","class"=>"form-control"],
									 @$row->hasil_kerja); ?>
		</div>
    </div>
	<div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Jumlah Beban <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<?= _input('jumlah_beban',['class'=>'form-control col-md-12 col-xs-12'],@$row->jumlah_beban,'number'); ?>
		</div>
    </div>
	<div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Waktu Penyelesaian <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
			<?= _input('waktu_penyelesaian',['class'=>'form-control col-md-12 col-xs-12'],@$row->waktu_penyelesaian,'number'); ?>
		</div>
    </div>
	
</form>

<script>
	var form = $('#fm');
		form.validate({
			rules:{
				id_jabatan : {
					required:true,
					
				},
				tugas_pokok : {
					required:true,
				},
				hasil_kerja : {
					required:true
				},
				jumlah_beban : {
					required:true,
					number:true
				},
				waktu_penyelesaian : {
					required:true,
					number:true
				},
				
			}
		})
	function saveForm()
	{
		if(form.valid()){
			$.post('<?= site_url('tugas_pokok_jabatan/save'); ?>',form.serialize()).done(function(result){
				if (result == 'success'){
					$.alert('Data Berhasil disimpan');
					$('#myModal').modal('hide');
					table.draw();
				} else {
					$.alert(result);
				}
			}).fail(function(xhr){
				$.alert(xhr.responseText);
			});
		}
	}
</script>