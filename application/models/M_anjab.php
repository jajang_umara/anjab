<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_anjab extends MY_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->table = 'anjab';
		$this->column_order = ['id_jabatan'];
		$this->order = ['id_jabatan'=>'asc'];
	}
	
	function filter()
	{
		if (session('role') == 2){
			$this->db->where('jpt_madya',session('unit_id'));
		}
	}
	
	function generate_datatable()
	{
		$select = $this->table.'.*,master_jabatan.nama_jabatan,master_unit_kerja.unit_kerja';
		$join   = [['master_jabatan','master_jabatan.id = '.$this->table.".id_jabatan"],
					['master_unit_kerja','master_unit_kerja.id = '.$this->table.".id_unit_kerja"]];
		$result = $this->get_datatables($select,$join);
		$rows = $result['data'];
		if ($rows){
			$no = $_POST['start'] + 1;
			foreach ($rows as $key=>$row){
				$rows[$key]->action = " <a href='".site_url('anjab/detail/'.$row->id)."' class='btn btn-primary btn-sm' data-toggle='tooltip' data-placement='top' title='Detail Data'><i class='fa fa-eye'></i></a>
										<a href='".site_url('anjab/form/'.$row->id)."' class='btn btn-info btn-sm' data-toggle='tooltip' data-placement='top' title='Ubah Data'><i class='fa fa-pencil'></i></a>
									   <button class='btn btn-danger btn-sm' data-container='table' data-toggle='tooltip' data-placement='top' title='Hapus Data' onclick='deleteData(".$row->id.")'><i class='fa fa-trash'></i></button>";
				$rows[$key]->no     = $no;
				$no++;
			}
		}
		$result['data'] = $rows;
		return $result;
	}
	
	function get_full_jabatan($id)
	{
		$row = $this->db->select('A.*,B.nama_jabatan,B.kode_jabatan,B.deskripsi_jabatan,B.eselon,C.unit_kerja as nama_jpt_madya,D.unit_kerja as nama_jpt_pratama,E.unit_kerja as nama_administrator,F.unit_kerja as nama_pengawas,
								 G.unit_kerja as nama_pelaksana')->from($this->table." A")
						->join('master_jabatan B','A.id_jabatan=B.id','left')
						->join('master_unit_kerja C','A.jpt_madya=C.id','left')
						->join('master_unit_kerja D','A.jpt_pratama=D.id','left')
						->join('master_unit_kerja E','A.administrator=E.id','left')
						->join('master_unit_kerja F','A.pengawas=F.id','left')
						->join('master_unit_kerja G','A.pelaksana=G.id','left')
						->where('A.id',$id)
						->get()
						->row();
		return $row;
	}
	
	function get_parent_child($id,$parent=0,$result = array())
	{
		
		if ($parent == 0){
			$rows = $this->db->select('A.*,C.nama_jabatan,C.eselon,B.jumlah_kebutuhan,B.jumlah_saat_ini')->from('master_unit_kerja A')
							 ->join('anjab B','A.id=B.id_unit_kerja','left')
							 ->join('master_jabatan C','B.id_jabatan=C.id','left')
							 ->where('A.id',$id)->get()->result_array();
		} else {
			$rows = $this->db->select('A.*,C.nama_jabatan,C.eselon,B.jumlah_kebutuhan,B.jumlah_saat_ini')->from('master_unit_kerja A')
							 ->join('anjab B','A.id=B.id_unit_kerja','left')
							 ->join('master_jabatan C','B.id_jabatan=C.id','left')
							 ->where('A.parent',$parent)->get()->result_array();
		}
		
		if ($rows){
			foreach($rows as $row){
				$result[] = $row;
				$result = $this->get_parent_child($id,$row['id'],$result);
			}
		}
		return $result;
	}
	
	
}